// src/List/index.tsx
import React from 'react';
import { useFavorites } from '../FavoritesContext';

interface ListItem {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

interface ListProps {
  items: ListItem[];
}

const List: React.FC<ListProps> = ({ items }) => {
  const { favorites, addFavorite, removeFavorite } = useFavorites();

  return (
    <div className="list-container">
      <div className="grid-container">
        {items.map(item => {
          const isFavorite = favorites.some(favoriteItem => favoriteItem.id === item.id);
          return (
            <div key={item.id} className="grid-item">
              <p>Album ID: {item.albumId}</p>
              <p>ID: {item.id}</p>
              <p>Title: {item.title}</p>
              <img src={item.thumbnailUrl} alt={item.title} />
              <p>URL: <a href={item.url} target="_blank" rel="noopener noreferrer">{item.url}</a></p>
              {isFavorite ? (
                <button onClick={() => removeFavorite(item.id)}>Remove from Favorites</button>
              ) : (
                <button onClick={() => addFavorite(item)}>Add to Favorites</button>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default List;