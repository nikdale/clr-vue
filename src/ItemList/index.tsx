import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import List from '../List';
import BackButton from '../BackButton';

interface Photo {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

const ItemList: React.FC = () => {
  const [items, setItems] = useState<Photo[]>(JSON.parse(localStorage.getItem('items') || '[]'));
  const [page, setPage] = useState(Number(localStorage.getItem('page')) || 1);
  const [loading, setLoading] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);

  const fetchItems = (page: number) => {
    setLoading(true);
    axios
      .get(`https://jsonplaceholder.typicode.com/albums/1/photos?_page=${page}&_limit=10`)
      .then(response => {
        const newItems = response.data.filter((newItem: Photo) => {
          return !items.some(existingItem => 
            existingItem.albumId === newItem.albumId && existingItem.id === newItem.id
          );
        });
        setItems([...items, ...newItems]);
        localStorage.setItem('items', JSON.stringify([...items, ...newItems]));
        setLoading(false);
      });
  };
  

  useEffect(() => {
    fetchItems(page);
  }, [page]);

  useEffect(() => {
    const savedScrollPosition = localStorage.getItem('scrollPosition');
    if (savedScrollPosition && containerRef.current) {
      containerRef.current.scrollTop = Number(savedScrollPosition);
    }
  }, [items]);

  const handleScroll = (event: React.UIEvent<HTMLDivElement>) => {
    const { scrollTop, clientHeight, scrollHeight } = event.currentTarget;
    localStorage.setItem('scrollPosition', String(scrollTop));
    if (!loading && scrollHeight - scrollTop === clientHeight) {
      const newPage = page + 1;
      setPage(newPage);
      localStorage.setItem('page', String(newPage));
    }
  };

  return (
    <div ref={containerRef} onScroll={handleScroll} className="item-list-container">
      <div className="back-button-container">
        <BackButton />
      </div>
      <div className="list-container">
        <List items={items} />
      </div>
      {loading && <div>Loading...</div>}
    </div>
  );
};

export default ItemList;
