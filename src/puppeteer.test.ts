import puppeteer, { Browser, Page } from "puppeteer";
import { createServer } from "http-server";

let browser: Browser;
let page: Page;
let server: any;

beforeAll(async () => {
  server = createServer({ root: "./build" });
  server.listen(8081);
  browser = await puppeteer.launch();
  page = await browser.newPage();
});

afterAll(async () => {
  await browser.close();
  server.close();
});

test("should navigate to /list, add to favorites, and check favorites", async () => {
  await page.goto("http://localhost:3000");

  await page.evaluate(() => {
    (
      document.querySelector("button:contains('Go to List')") as HTMLElement
    ).click();
  });

  await page.evaluate(() => {
    (
      document.querySelector(
        "button:contains('Add to Favorites')"
      ) as HTMLElement
    ).click();
  });

  await page.evaluate(() => {
    (
      document.querySelector(
        "button:contains('Back to Dashboard')"
      ) as HTMLElement
    ).click();
  });

  const favoriteItem = await page.waitForSelector("#favorite-item-5");
  expect(favoriteItem).toBeTruthy();
}, 30000);
