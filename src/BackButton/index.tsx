import React from 'react';
import { Link } from 'react-router-dom';

const BackButton: React.FC = () => (
  <div className="back-button-container"> {}
    <Link to="/">
      <button>Back to Dashboard</button>
    </Link>
  </div>
);

export default BackButton;