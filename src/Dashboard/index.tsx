import React from 'react';
import { Link } from 'react-router-dom';
import { useFavorites } from '../FavoritesContext';
import List from '../List';

const Dashboard: React.FC = () => {
  const { favorites } = useFavorites();

  return (
    <div className="dashboard-container">
      <Link to="/list">
        <button>Go to List</button>
      </Link>
      <h2>Favorites</h2>
      <List items={favorites} key={favorites.length} />
    </div>
  );
};

export default Dashboard;