import React, { createContext, useState, useContext, ReactNode } from 'react';

interface ListItem {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

interface FavoritesContextProps {
  favorites: ListItem[];
  addFavorite: (item: ListItem) => void;
  removeFavorite: (id: number) => void;
}

interface FavoritesProviderProps {
  children: ReactNode;
}

const FavoritesContext = createContext<FavoritesContextProps | undefined>(undefined);

export const FavoritesProvider: React.FC<FavoritesProviderProps> = ({ children }) => {
  const [favorites, setFavorites] = useState<ListItem[]>([]);

  const addFavorite = (item: ListItem) => {
    setFavorites(prevFavorites => [...prevFavorites, item]);
  };

  const removeFavorite = (id: number) => {
    setFavorites(prevFavorites => prevFavorites.filter(item => item.id !== id));
  };

  return (
    <FavoritesContext.Provider value={{ favorites, addFavorite, removeFavorite }}>
      {children}
    </FavoritesContext.Provider>
  );
};

export const useFavorites = () => {
  const context = useContext(FavoritesContext);
  if (!context) {
    throw new Error('useFavorites must be used within a FavoritesProvider');
  }
  return context;
};