import React from 'react';
import ReactDOM from "react-dom/client";
import { FavoritesProvider } from './FavoritesContext';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
    <React.StrictMode>
        <FavoritesProvider>
          <App />
        </FavoritesProvider>
    </React.StrictMode>
);