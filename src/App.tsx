import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import ItemList from './ItemList';
import './App.scss';    

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Dashboard />} />
        <Route path="/list" element={<ItemList />} /> {}
      </Routes>
    </Router>
  );
};

export default App;